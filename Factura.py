#!/usr/bin/env python
# coding: utf-8


import re
import requests
import pdfplumber
import pandas as pd
from collections import namedtuple
import os
import glob
import numpy as np
import calendar
import time
import sys


# pasar el directorio y el nombre del fichero como variables.

def load_items(items):
    RUT = re.compile(r'(^R.U.T.: )' )
    folio = re.compile(r'(^COMISIONISTA)' )
    comuna = re.compile(r'(^COMUNA)' )
    fecha = re.compile(r'(^Fecha)' )
    nombre = re.compile(r'(^SEÑ)' )
    total = re.compile(r'(^TOTAL )' ) 
    direccion = re.compile(r'(^DIREC)' )
    cilindro = re.compile(r'(^- [c,C])' )
    
    cylinders = [0] * 6  
    
    fecha_encontrada = False
    cliente = ''
    
    for line in text.split('\n'):
        if folio.match(line):
            line = line.replace('Nº', '')
            factura = re.findall('\d{4}', line)
            factura= ' '.join(factura)
            factura = int(factura)
            
        if(len(cliente)==0 and fecha_encontrada):
            if (line.find('SEÑOR') ==-1):
                print(line)
                largename = line
            fecha_encontrada = False

        if nombre.match(line):
            atributo, *valor = line.split()
            if (len(line)>0):
                cliente = ' '.join(valor)
                print(cliente)
        if fecha.match(line):
            fecha_encontrada = True
            atributo, *valor = line.split(':')
            date = (' '.join(valor)).lower()
            date = format_fecha(date)
        if RUT.match(line):
            new_string = re.sub(RUT, ' ', line)
            rut = new_string.replace(' ', '')
        if comuna.match(line):  
            pattern = re.compile('(?<=COMUNA).*?(?=CIUDAD:)')
            comuna_cliente = re.findall(pattern, line)
            comuna_cliente = (''.join(comuna_cliente)).strip()
            ciudad = line.split(':')
            ciudad = ciudad[1].strip()
        if total.match(line):
            line = line.replace('$', '')
            atributo, *valor = line.split()
            montoTotal= ' '.join(valor)
            montoTotal = int(montoTotal.replace(".", ''))
        if direccion.match(line):
            atributo, *valor = line.split()
            dir = ' '.join(valor)
        if cilindro.match(line):
            atributo, *valor = line.split()
            tubo = ' '.join(valor)
            tubo = tubo.upper()
        
            if (tubo.find("VM") == -1):
                (formato,cantidad) = [int(i) for i in tubo.split() if i.isdigit()] 
            if(tubo.find("VM") != -1):
                cantidad = [int(i) for i in tubo.split() if i.isdigit()] 
                formato = re.findall(r"VM\s\w+",tubo)
                if(len(formato) == 0):
                    formato = re.findall(r"VM[A|F]",tubo)
                formato = ''.join(formato)
                cantidad = cantidad[0]
            append_into_index(cylinders,formato,cantidad)
          
    fulldir = dir+', '+comuna_cliente+", "+ciudad
    if (len(cliente)>0):
        items.append(Inv(date, factura, cliente, fulldir,rut, cylinders[0], cylinders[1], cylinders[2], cylinders[3], cylinders[4],cylinders[5], montoTotal))
    else:
        items.append(Inv(date, factura, largename, fulldir,rut, cylinders[0], cylinders[1], cylinders[2], cylinders[3], cylinders[4],cylinders[5], montoTotal))
    print(cylinders)
            

def format_fecha(line):
    line = line.replace("del ",'')
    line = line.replace("de ", '')
    pattern = re.compile('\d{2}\s')
    day = re.findall(pattern, line)
    day = ''.join(day)
    day = day.strip()
    pattern = re.compile('\d{4}')
    year = re.findall(pattern, line)
    year = ''.join(year)
    pattern = re.compile('[a-z]+')
    month = re.findall(pattern, line)
    month = ''.join(month)
    month = month_string_to_number(month)
    if(month <10):
        month ='0'+str(month)   
    date = day+"/"+str(month)+"/"+year
    return date

def month_string_to_number(string):
    m = {
        "ene": 1,
        "feb": 2,
        "mar": 3,
        "abr":4,
        "may":5,
        "jun":6,
        "jul":7,
        "ago":8,
        "sep":9,
        "oct":10,
        "nov":11,
        "dic":12
    }
    s = string.strip()[:3].lower()

    try:
        out = m[s]
        return out
    except:
        raise ValueError("Not a month")

def append_into_index(array, formato, cantidad): 
    if(formato == 5):
        if (array[0] != '0'):
            previous_quantity = array[0]
            cantidad = cantidad+previous_quantity
            array[0] = cantidad
        array[0] = cantidad
    
    if(formato == 11):
        if (array[1] != '0'):
            previous_quantity = array[1]
            cantidad = cantidad+previous_quantity
            array[1] = cantidad
        array[1] = cantidad
   
    if (formato == 15):
        if (array[2] != '0'):
            previous_quantity = array[2]
            cantidad = cantidad+previous_quantity
            array[2] = cantidad
        array[2] = cantidad
        
    if(formato == 45):
        if (array[3] != '0'):
            previous_quantity = array[3]
            cantidad = cantidad+previous_quantity
            array[3] = cantidad
        array[3] = cantidad
        
    if(formato == "VM FIERRO" or formato == "VMF"):
        if (array[4] != '0'):
            previous_quantity = array[4]
            cantidad = cantidad+previous_quantity
            array[4] = cantidad
        array[4] = cantidad
        
    if(formato == "VM ALUMINIO" or formato == "VMA"):
        if (array[5] != '0'):
            previous_quantity = array[5]
            cantidad = cantidad+previous_quantity
            array[5] = cantidad
        array[5] = cantidad

start_time = time.time()
filepath = "\\facturas\\"
mainpath = os.path.abspath(os.path.join(os.getcwd(), os.pardir)) + filepath
os.chdir(mainpath)
items = []
root = os.path.abspath(os.path.join(os.getcwd(), os.pardir)) + "\\"
Inv = namedtuple('Inv', 'Fecha Factura Cliente Direccion RUT N5 N11 N15 N45 VM_Fierro VM_Aluminio Total')
for file in glob.glob("*.pdf"):
    fullpath= os.path.join(mainpath, file) 
    print(fullpath)
    with pdfplumber.open(fullpath) as pdf:
        page = pdf.pages[0]
        text = page.extract_text()
        load_items(items)
dataframe = pd.DataFrame(items)
dataframe = dataframe.sort_values(by=['Factura'])
dataframe.to_excel(root + "Facturacion.xlsx", index=False, sheet_name= "PLANILLA")

print("\n###################### THE WORK IS DONE! ########################\n")
print("--- Execution Time:  %s seconds " % (time.time() - start_time))
print("--- Developed by Héctor Astorga T. ")
print("\n#################################################################")
time.sleep(3)
